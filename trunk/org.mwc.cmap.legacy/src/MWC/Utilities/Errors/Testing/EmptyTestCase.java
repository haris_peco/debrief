package MWC.Utilities.Errors.Testing;

import junit.framework.TestCase;

/**
 */
public class EmptyTestCase extends TestCase {
  /**
  * Basic constructor - called by the test runners.
  */
  public EmptyTestCase(final String s) {
    super (s);
  }

  public void testDummy () {}
}