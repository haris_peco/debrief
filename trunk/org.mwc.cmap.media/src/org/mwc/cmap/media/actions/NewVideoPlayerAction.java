package org.mwc.cmap.media.actions;

import org.mwc.cmap.media.views.VideoPlayerView;

public class NewVideoPlayerAction extends BasePlanetmayoAction {

	public NewVideoPlayerAction() {
		super(VideoPlayerView.ID);
	}
}
