<?xml version="1.0" encoding="UTF-8"?>
<cheatsheet xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xsi:noNamespaceSchemaLocation="../contentFile.xsd" title="Importing solution">
   <intro>
      <description>
         Debrief has a range of analysis and export capabilities that work on <b>Track</b> objects. 
         The current <b>Solution</b> isn&apos;t yet a track, so
         we have to import it.
      </description>
   </intro>
   <item
         title="Select scenario">
      <description>
         If you&apos;ve followed the instructions correctly then you will have a <b>Scenario</b> named <b>Single Leg</b> in your <b>Layer Manager</b> view.
         Select it.
      </description>
   </item>
   <item
      title="Import as Composite Track">
      <description>
         The first way of importing, is to convert the current solution to a <b>Composite Track</b>. Just right-click on the scenario in the Layer Manager,
          and select <b>Convert to Composite Track (legs)</b>.
         <br/>Once you&apos;ve done that, you will see a new track appear on the plot (it&apos;s probably yellow). You will also see
          a new item in the <b>Layer Manager</b> - it will have the same name as the scenario, but it will be marked as a <b>Track</b>.
      </description>
   </item>
   <item
      title="Rename to avoid confusion">
      <description>
         To prevent problems later on, we&apos;ll now rename the imported track. So, select the <b>Track:Single Leg_0</b> track in 
         the <b>Layer Manager</b>, then change its name to <b>Single Leg TMA</b> in the <b>Properties</b> view. 
         
      </description>
   </item>
   <item
      title="Tidy the plot">
      <description>
         Now, it would be all to easy for the plot to get confused here. So, use the <b>Layer Manager</b> to hide the <b>Single Leg</b> scenario, and 
         the <b>SUBJECT</b> track.
      </description>
   </item>
   <item
      title="Manually tune the TMA solution">
      <description>
         So, we should now just have the <b>OWNSHIP</b> and <b>Single Leg_0</b> tracks visible.  Let&apos;s try adjusting the solution. We 
         do this just like we do for normal Single-Sided Reconstruction: click on the <b>Drag TMA Segment</b> arrow button in the Debrief toolbar.
         <br/>As you click it, you will see the <b>Bearing Residuals</b> view open.  Next, make the <b>OWNSHIP</b> track as the Primary track, and 
         the <b>Single Leg TMA</b> track as the secondary track (using the mini-toolbar above the <b>Layer Manager</b>). Now you can drag the yellow track
         and see the error residuals move.  Note: the <b>Asbolute</b> values graph is easier to read in this instance if you select the <b>Use +/- 180 scale
         for the Absolute Data</b> button (it&apos;s probably the second button along).
      </description>
   </item>
   <item
      title="Import as standalone track">
      <description>
         Now, all that manual track fine-tuning is more suited to complex scenarios where you, the analyst remains unhappy with the SATC solution,
         culminating in the analyst merging the TMA solutions into a formal track.<br/>
         But, in this case, the raw SATC produced a perfectly acceptable solution. So, let&apos;s ditch the manual TMA track (by right-clicking on <b>Single Leg TMA</b> and selecting 
         <b>Delete Single Leg TMA</b>. Intead let's just import our original TMA solution by right-clicking on the <b>Single Leg</b> scenario, and selecting <b>Convert to Standalone Track</b>.
      </description>
   </item>
</cheatsheet>