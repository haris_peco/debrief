package com.planetmayo.debrief.satc.model.legs;

public enum AlteringRouteType
{
	UNDEFINED,
	QUAD_BEZIER,
	CUBIC_BEZIER
}
