package com.planetmayo.debrief.satc.model.manager;

import java.util.List;

import com.planetmayo.debrief.satc.model.VehicleType;

public interface IVehicleTypesManager
{

	List<VehicleType> getAllTypes();
}
